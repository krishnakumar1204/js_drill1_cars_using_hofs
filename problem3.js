// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.



const sortCarModels = (inventory)=> {
    if(Array.isArray(inventory)){
        let carModels = inventory.map(car => car.car_model);

        carModels = carModels.sort((carModel1, carModel2) => {
            if(carModel1.toLowerCase() < carModel2.toLowerCase()){
                return -1;
            }
            else if(carModel1.toLowerCase() > carModel2.toLowerCase()){
                return 1;
            }
            else{
                return 0;
            }
        });

        return carModels
    }
    else{
        return [];
    }
}

module.exports = sortCarModels;