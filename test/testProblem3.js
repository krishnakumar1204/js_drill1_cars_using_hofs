const inventory = require("../data.js");
const sortCarModels = require("../problem3.js");

try {
    let carModels = sortCarModels(inventory);
    console.log((carModels));
} catch (error) {
    console.log("Something went wrong");
}