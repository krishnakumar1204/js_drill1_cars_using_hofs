const inventory = require("../data");
const findCarYears = require("../problem4");


try {
    let carYears = findCarYears(inventory);
    console.log(carYears);
} catch (error) {
    console.log("Something went wrong");
}