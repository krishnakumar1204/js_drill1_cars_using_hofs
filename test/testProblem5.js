const inventory = require("../data");
const findCarsOlderThan2000 = require("../problem5");


try {
    let carsOlderThan2000 = findCarsOlderThan2000(inventory);
    console.log(carsOlderThan2000.length);
} catch (error) {
    console.log("Something went wrong");
}