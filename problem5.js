// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const findCarYears = require("./problem4");


const findCarsOlderThan2000 = (inventory) => {
    if(Array.isArray(inventory)){
        let carYears = findCarYears(inventory);
        let carsOlderThan2000 = carYears.filter((carYear) => carYear<2000);
        return carsOlderThan2000;
    }
    else{
        return [];
    }
}

module.exports = findCarsOlderThan2000;